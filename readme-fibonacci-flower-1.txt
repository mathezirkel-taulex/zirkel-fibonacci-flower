Zirkel (Fibonacci-Flower 1), erster voller Präsenzzirkel Klasse 7/8 2022/23, neu erstellt. Von Alex Mai.

Zirkel grob in 3 Teile aufgeteilt:
1) über Winkel bei pflanzen für effiziente Platznutzung nachgedacht, mit Visualisierung durch Geogebra-Simulation
2) Blatt zu Fibonacci-Zahlen gemacht
3) kurz noch Spiralen in Blumen angeschaut


Fazit:

Bei der Simulation gemeinsam mit verschiedenen Winkeln zu experimentieren und die resultierenden Bilder zu sehen schien interessant gewesen zu sein für die Kinder. Waren gut dabei herauszufinden, welche Regeln für die Winkel gut sind.

Die Aufgaben auf dem Blatt waren genau richtig. Auch etwas fittere Kinder hatten viel Spaß mit den Bonus(Beweis)aufgaben "Jede 3. Fib-Zahl ist gerade" und "Wann sind zwei Folgen mit der Fib-Regel identisch?" -> wurden dann auch an der Tafel von den Kindern vorgestellt. Die Bonusaufgabe zum finden einstelliger Startwerte für vorgegebene spätere Zahlen wurde nicht gemacht, war aber auch fürs Knobeln daheim gedacht.

In zukünftigen Jahren wäre zu überlegen, ob die Struktur geändert werden sollte: erst Fibonacci-Zahlen (die an sich alleine schon recht spaßig sind), dann noch genug Zeit für Goldenen Schnitt, und dann im zweiten Teil alles mit Blumen und Spiralen. Gerade unschlüssig, was besser ist.


Zum Ablauf:

1) Zuerst haben wir uns überlegt, in welchen Winkeln die Kerne in der Mitte mancher Blumen (siehe Blatt Seite 3 für Bilder) wachsen müssen, um möglichst viel von der Fläche zu nutzen. Mithilfe einer GeoGebra-Simulation haben wir am Winkel rumgespielt. Dabei immer konkrete Eingaben. Die Kinder fanden mit etwas Hilfe dann fix raus, dass ein kleinerer Winkel zu mehr genutzter Fläche führt. Bei Winkeln der Form (1/k) * 360° entstehen genau k viele Arme. Ein größerer Zähler, vor allem teilerfremd zu k, verteilt alles schonmal besser. Idealerweise ist der Zähler auch nicht nah dran an einem großen Teiler des Nenners. Wir kommen schließlich darauf, dass irrationale Zahlen ganz gut wären, aber es ist erstmal unklar, welche irrationale Zahl.

2) Dann Sprung zu Fibonacci-Zahlen und Bearbeitung des Arbeitsblattes, zumindest Seite 1 und 2. Beim ersten Mal, als dieser Zirkel gehalten wurde, war am Ende keine Zeit mehr für die letzte Aufgabe der zweiten Seite, die den goldenen Schnitt ins Spiel bringt.

3) Stattdessen schauten wir uns noch kurz die Bilder auf Seite 3 gemeinsam an und haben die Spiralarme gefunden. Dabei haben die Kinder die weiter drinnen liegenden nicht direkt gefunden, erst nach Zeigen am Beamer. Als freiwillige Hausaufgabe bekamen sie auf, die Spiralarme zu zählen. Teil 2 des Zirkels beginnt dann mit einer Auflösung, wie viele Spiralarme da sind.
