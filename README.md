# zirkel-fibonacci-flower

Exercises on the Fibonacci Numbers and GeoGebra simulation of flower seeds growing.

Great video on Fibonacci on flowers: [Mathologer – The fabulous Fibonacci flower formula](https://www.youtube.com/watch?v=_GkxCIW46to).

With inspiration from [Ingo Blechschmidt's talk 'The secret of the number 5'](https://github.com/iblech/number5).