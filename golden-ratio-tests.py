import math

options = """
Testing some golden ratio stuff.
  f -> print some Fibonacci numbers, starting with 1, 1
  fq -> print fib successive quotients
  fqg -> print fib quotients difference from golden angle Phi = (1 + sqrt(5)) / 2

  l -> print some Lucas numbers, starting with 1, 3
  lq -> print lucas successive quotients
  lqg -> print lucas quotients difference from golden angle Phi

  Or enter 2 integers comma-separated -> get their sequence, quotients, differences.
"""

# some preparations
phi = (1 + math.sqrt(5)) / 2
it_max = 50

def get_sequence(sequence):
  if len(sequence) <= 2:
    for i in range(len(sequence), it_max):
      sequence.append(sequence[-1] + sequence[-2])
  return sequence


def get_quotients(sequence, quotients):
  seq = get_sequence(sequence)
  if len(quotients) <= 2:
    quotients = get_new_quotients(seq)
  return quotients

def get_new_quotients(seq):
  quots = []
  for i in range(len(seq)-1):
    quots.append(seq[i+1] / seq[i])
  return quots


def get_diffs(quotients):
  diffs = [(x - phi) for x in quotients]
  return diffs


def print_list(li):
  result = ""
  for i in range(len(li)):
    result = f"{result}\n{i}: {li[i]}"
  print(result)


def main():

  # get first 102 fibonacci numbers
  fibs = [1, 1]
  fibs_quotients = []
  fibs_quotients_diffs = []

  # get first 102 lucas numbers
  lucas = [1, 3]
  lucas_quotients = []
  lucas_quotients_diffs = []

  print(options)

  while True:
    selection = input("\n\nChoose option:\nh -> help\nq -> exit\nf, fq, fqg, l, lq, lqg, or enter 2 integers.\n")

    if selection == 'h':
      print(options)
    elif selection == 'q':
      break
    elif selection == 'f':
      fibs = get_sequence(fibs)
      print_list(fibs)
    elif selection == 'fq':
      fibs = get_sequence(fibs)
      fibs_quotients = get_quotients(fibs, fibs_quotients)
      print_list(fibs_quotients)
    elif selection == 'fqg':
      fibs = get_sequence(fibs)
      fibs_quotients = get_quotients(fibs, fibs_quotients)
      fibs_quotients_diffs = get_diffs(fibs_quotients)
      print_list(fibs_quotients_diffs)
    elif selection == 'l':
      lucas = get_sequence(lucas)
      print_list(lucas)
    elif selection == 'lq':
      lucas = get_sequence(lucas)
      lucas_quotients = get_quotients(lucas, lucas_quotients)
      print_list(lucas_quotients)
    elif selection == 'lqg':
      lucas = get_sequence(lucas)
      lucas_quotients = get_quotients(lucas, lucas_quotients)
      lucas_quotients_diffs = get_diffs(lucas_quotients)
      print_list(lucas_quotients_diffs)
    else:
      try:
        start1 = int(selection.split(",")[0].strip())
        start2 = int(selection.split(",")[1].strip())
        new_seq = get_sequence([start1, start2])
        new_quots = get_new_quotients(new_seq)
        new_diffs = get_diffs(new_quots)
        
        print(f"\n\nSequence ({it_max} digits)")
        print_list(new_seq)
        print(f"\n\nQuotients")
        print_list(new_quots)
        print(f"\n\nDifferences to Phi")
        print_list(new_diffs)
      except ValueError:
        print("try again")


if __name__ == "__main__":
    main()
